let contextMenus = {};

contextMenus.copyRequestPayload = chrome.contextMenus.create({'title': 'copy request payload'}, () => {
    if (chrome.runtime.lastError) {
        console.log(`Run Time Error ${chrome.runtime.lastError.message}`);
    }
});

chrome.contextMenus.onClicked.addListener(copyRequestHandler);

function copyRequestHandler(info, tab) {
    if(info.menuItemId === contextMenus.copyRequestPayload) {
        chrome.tabs.executeScript(tab.id, {file: "script/copyRequestPayload.js"});
    }
}