try {
    copyToClipboard(
      document
        .querySelector("[data-macro-id='6e2aa6b9-c305-4cd1-b549-fe18165c8c2e']")
        .querySelector("[class='panelContent']")
        .textContent.replace(/[\n\r]+|[\s]{2,}/g, "")
        .replace(/\s+/g, "")
    );
  } catch {
    console.log("Init copy function() ...");
    const copyToClipboard = str => {
      const el = document.createElement("textarea");
      el.value = str;
      document.body.appendChild(el);
      el.select();
      document.execCommand("copy");
      document.body.removeChild(el);
  
      window.alert("Request Payload copied!");
    };
  
    copyToClipboard(
      document
        .querySelector("[data-macro-id='6e2aa6b9-c305-4cd1-b549-fe18165c8c2e']")
        .querySelector("[class='panelContent']")
        .textContent.replace(/[\n\r]+|[\s+]{2,}/g, "")
        .replace(/\s+/g, "")
    );
  }
  